<?php
/**
 * @file
 * Contains \Drupal\video_filter_html5\Plugin\VideoFilter\Html5Codec.
 */

namespace Drupal\video_filter_html5\Plugin\VideoFilter;

use Drupal\video_filter\VideoFilterBase;
use Drupal\file\Entity\File;

/**
 * Provides Html5Codec codec for Video Filter
 *
 * @VideoFilter(
 *   id = "html5",
 *   name = @Translation("HTML5"),
 *   url = "http://www.site.org/path/to/file.mp4",
 *   regexp = {
 *     "/^(.*\.(?:mp4|m4v|webm))$/i",
 *   },
 *   ratio = "4/3",
 * )
 */
class Html5Codec extends VideoFilterBase {

  /**
   * {@inheritdoc}
   */
  public function html($video) {
    $file_url = $video['codec']['matches'][1];

    $parts = explode('?', $file_url);
    $parts = explode('.', $parts[0]);
    $ext = $parts[count($parts) - 1];
    $type = 'video/mp4';
    switch ($ext){
      case 'm4v':
      case 'mp4':
        $type = 'video/mp4';
        break;
      case 'webm':
        $type = 'video/webm';
        break;
    }
    $poster = '';
    if (!empty($video['poster'])){

      $file = File::load($video['poster']);
      if (!empty($file)){
        $poster = file_create_url($file->getFileUri());
      }
    }
    $html =
      '<video '.
        (!empty($video['width']) && $video['width'] != 0 ? 'width="'.$video['width'].'" ' : 'width="100%" ').
        (!empty($video['height']) && $video['height'] != 0 ? 'height="'.$video['height'].'" ' : 'height="auto" ').
        (!empty($poster) ? 'poster="'.$poster.'" ' : '').
      'controls >'.
        '<source type="'.$type.'" src="'.$file_url.'">'.
      '</video>';
    return $html;
  }

  /**
   * {@inheritdoc}
   */
  public function options() {
    $video_filter_html5_config = \Drupal::config('video_filter_html5.settings');

    $form['width'] = [
      '#title' => $this->t('Width (optional)'),
      '#type' => 'textfield',
    ];
    $form['height'] = [
      '#title' => $this->t('Height (optional)'),
      '#type' => 'textfield'
    ];
    $form['poster'] = array(
      '#title' => t('Poster Image'),
      '#type' => 'managed_file',
      '#description' => t('The uploaded image will be displayed on this page using the image style chosen below.'),
      '#upload_location' => $video_filter_html5_config->get('poster.upload_location'),
    );
    return $form;
  }

}
