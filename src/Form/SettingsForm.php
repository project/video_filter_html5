<?php

/**
 * @file
 * Contains Drupal\video_filter_html5\Form\SettingsForm.
 */
namespace Drupal\video_filter_html5\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\video_filter_html5\Port\PluginManager;
/**
 *
 * @see \Drupal\Core\Form\FormBase
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'video_filter_html5.settings',
    ];
  }

  /**
   * Build the HTML5 video settings form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('video_filter_html5.settings');

    $form['poster_upload_location'] = [
      '#title' => $this->t('Poster Upload Location'),
      '#type' => 'textfield',
      '#default_value' => $config->get('poster.upload_location')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller.  it must
   * be unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'video_filter_html5_settings_form';
  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('video_filter_html5.settings')
      ->set('poster.upload_location', $form_state->getValue('poster_upload_location'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
