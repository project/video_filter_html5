This module adds support for direct URLs to video files using [Video Filter](https://www.drupal.org/project/video_filter) module.
The provided filter detects URLs that ends with .m4v/.mp4/.webm and displays them as HTML5 video tags.
In the settings form you can set width and height and pick image file for a poster.

##Configuration

The directory for uploading poster images can be changed at  *Administration / Configuration / Content authoring / HTML Video Filter Settings* (`/admin/config/content/video_filter_html5`).
